"""
Check for signs that a submission may include identifying information, using the paperall API

"""
from urllib.request import urlopen
import json
import pprint
import re


def check_for_pii(value):
    """
    Check a particular field's value for PII.

    Return a tuple describing the first found PII violation, or False
    if none are found.
    """
    if not value:
        return False

    checks = [
        "@(?!(?:classmethod|staticmethod|property|attr))",
        "github.com",
        "speakerdeck.com",
        "(my|our) name",
        "(my|our) blog",
        "website",
    ]

    value = value.lower()
    for check in checks:
        spots = list(re.finditer(check, value))
        if spots:
            s = spots[0].start()
            e = spots[0].end()
            excerpt = value[s-10:e+10]
            return (check, "...{}...".format(excerpt))

    return False


def check_talk_for_pii(cfp_fields):
    """
    Check a talk for possible PII leaks.

    A talk is a dictionary keyed by CFP field names whose values are
    the raw text of the CFP field value.
    """
    errs = []
    for field, val in cfp_fields.items():
        err = check_for_pii(val)
        if err:
            check, excerpt = err
            msg = "in {}: matched {} {!r}".format(field, check, excerpt)
            errs.append(msg)

    return errs


def papercall_talk_fields(talk):
    """
    Extract interesting talk fields from a Papercall talk
    """
    def getfield(fieldspec):
        path = fieldspec.split(".")
        elem = talk
        while path:
            key = path.pop(0)
            elem = elem.get(key, "")
        return elem

    return {
        "description": getfield("talk.description"),
        # "notes": getfield("talk.notes"),
        "additional_info": getfield("additional_info"),
        "abstract": getfield("talk.abstract"),
        "title": getfield("talk.title"),
    }


def papercall_submissions(api_key):
    """
    Yield submissions from the Papercall API
    """
    base_url = (
        "https://www.papercall.io/api/v1/submissions?"
        "order=created_at&"
        "per_page=20&"
        "state=submitted&"
        "_token={api_key}"
    ).format(api_key=api_key)

    page = 0
    while True:
        page += 1

        response = urlopen("{}&page={}".format(base_url, page))
        content = response.read()

        data = json.loads(content)
        if not data:
            break

        for talk in data:
            yield talk


def check_papercall_for_pii(api_key, whitelist):
    """
    Main entry point for papercall-based CFPs.
    """
    for talk in papercall_submissions(api_key):
        if talk["id"] in whitelist:
            continue
        errs = check_talk_for_pii(papercall_talk_fields(talk))
        if errs:
            url = "https://www.papercall.io/cfps/932/submissions/{}".format(talk["id"])
            print(url, errs)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Check CFP submissions for possibly identifying information")
    parser.add_argument("--papercall", dest="papercall_api_key", metavar="API_KEY")
    parser.add_argument("--whitelist", metavar="TALK_IDS", help="Comma-separated list of talk IDs to whitelist", default="")

    options = parser.parse_args()
    whitelist = frozenset([int(talk_id) for talk_id in options.whitelist.split(",") if talk_id])
    if options.papercall_api_key:
        check_papercall_for_pii(options.papercall_api_key, whitelist)
    else:
        parser.print_help()
        sys.exit(1)
