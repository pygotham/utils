# Anonymous CFP Checker

A tool that makes some simple checks to help ensure that conference talk
submissions do not contain identifying information.

Among other things, this checks for github links and phrases like "my
website".

Requres Python 3.
