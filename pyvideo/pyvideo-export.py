"""
Create or update a directory of JSON files for PyVideo.org from a
directory of Jekyll-style talk descriptions in Markdown format.

usage: see python pyvideo-export.py

PYVIDEO_DIR should be the local checkout of the PyVideo data repository.
This will create/update a directory of files for the talks, e.g.
"$PYVIDEO_DIR/pygotham-2017/videos/".

Only talks with a video_url field in their frontmatter will be imported.
"""
import argparse
import json
import os
import os.path
import re
import sys

import frontmatter

parser = argparse.ArgumentParser(
    description="Export talks for PyVideo.org",
    epilog=(
        "The OUTPUT_DIR should be the directory you want to contain the JSON "
        "files itself, e.g. '/path/to/pyvideo/data/pygotham-2017/videos/'."
    ),
)
parser.add_argument(
    "talks_dir", metavar="TALKS_DIR", help="Directory containing one .md file per talk"
)
parser.add_argument(
    "output_dir",
    metavar="OUTPUT_DIR",
    help="Directory into which to place the JSON output files",
)
opts = parser.parse_args()


for talk in os.listdir(opts.talks_dir):
    if not talk.endswith(".md"):
        continue

    talkpath = os.path.join(opts.talks_dir, talk)
    with open(talkpath) as fp:
        parsed = frontmatter.load(fp)

    if parsed.get("type", "").lower() not in ("talk", "keynote"):
        continue

    slug, _ = os.path.splitext(talk)

    url = parsed.get("video_url")
    if url is None:
        print("Skipping", parsed["title"], "no video_url")
        continue

    m = re.match("https://youtu.be/(.*)", url)
    if not m:
        print(
            "Skipping",
            parsed["title"],
            "could not parse video URL (expected https://youtu.be/...)",
        )

    vid = m.group(1)

    doc = {
        "copyright_text": "Creative Commons Attribution license (reuse allowed)",
        "description": parsed.content,
        "duration": parsed["duration"] * 60,
        "language": "eng",
        "recorded": parsed["slot"].strftime("%Y-%m-%d"),  # "2016-07-16",
        "related_urls": [
            "https://2021.pygotham.tv/talks/%s/" % slug,
        ],
        "speakers": parsed["speakers"],
        "tags": [],
        "thumbnail_url": "https://i.ytimg.com/vi/%s/maxresdefault.jpg" % vid,
        "title": parsed["title"],
        "videos": [
            {
                "type": "youtube",
                "url": url,
            }
        ],
    }

    filename = slug + ".json"
    outpath = os.path.join(opts.output_dir, filename)
    with open(outpath, "w") as fp:
        json.dump(doc, fp, indent=2)
        fp.write("\n")
