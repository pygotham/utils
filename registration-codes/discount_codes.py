#!/usr/bin/env python

from contextlib import suppress
import csv
import random
import string
import sys

from argh import arg, dispatch_commands
from argh.exceptions import CommandError
from argh.interaction import confirm, safe_input
from decouple import config
import requests
from tabulate import tabulate


EVENTBRITE_TOKEN = config('EVENTBRITE_TOKEN')
EVENTBRITE_API_BASE = 'https://www.eventbriteapi.com/v3'


def _ask(prompt, cast=str):
    """Prompt the user for input, casting it to the desired type."""
    answer = None
    while answer is None:
        raw_answer = safe_input(prompt).strip()
        # Try to cast the answer to the desired type.
        # If we can't, simply ask again.
        with suppress(ValueError):
            answer = cast(raw_answer)
    return answer


def _make_session():
    """Create and return an authenticated API session."""
    # Some detials stolen from
    # https://github.com/eventbrite/eventbrite-sdk-python/blob/05680e750e9b5346f95c73c299a2efdbb197cfd0/eventbrite/client.py#L33
    session = requests.Session()
    session.headers.update({
        'content-type': 'application/json',
        'Authorization': f'Bearer {EVENTBRITE_TOKEN}',
    })
    return session


def _choose_event(session):
    """Return the event to operate on.

    If only one event exists for the user specified by the API token,
    return it. Otherwise, prompt the user to choose one.
    """
    events = session.get(
        f'{EVENTBRITE_API_BASE}/users/me/owned_events/',
        json={'status': 'live'},
    ).json()['events']
    events = dict(enumerate(events))

    if not events:
        raise CommandError(
            'No events found. Please check your eventbrite token.')

    elif len(events) == 1:
        event = events[0]

    else:
        prompts = [f'Found {len(events)}. Please choose one.']
        for i, event_option in events.items():
            prompts.append(f'[{i}]: {event_option["name"]["text"]}')
        prompts.append('Select an event: ')
        choice = None
        while choice not in events:
            choice = _ask('\n'.join(prompts), cast=int)
        event = events[choice]
    return event


def generate():
    """Generate new discount codes for the specified event."""
    session = _make_session()
    event = _choose_event(session)

    ticket_classes = dict(enumerate(session.get(
        f'{EVENTBRITE_API_BASE}/events/{event["id"]}/ticket_classes/',
    ).json()['ticket_classes']))
    prompts = [
        f'Found {len(ticket_classes)} ticket classes. Please choose one.']
    for i, class_option in ticket_classes.items():
        prompts.append(f'[{i}]: {class_option["name"]}')
    prompts.append('Select a ticket class: ')
    choice = None
    while choice not in ticket_classes:
        choice = _ask('\n'.join(prompts), cast=int)
    ticket_class = ticket_classes[choice]

    if ticket_class['hidden']:
        discount_options = {'discount.type': 'access'}
        discount_confirmation = 'Type: access code'
    else:
        discount_percentage = _ask('Percentage off discount: ', cast=int)
        discount_options = {
            'discount.type': 'coded',
            'discount.percent_off': discount_percentage,
        }
        discount_confirmation = f'Type: {discount_percentage}% discount'

    prefix = _ask('Enter a prefix for your discount codes [a-zA-Z-]: ')

    quantity = _ask('How many codes do you need? ', cast=int)

    uses = _ask('How many uses should each code be good for? ', cast=int)

    if not confirm('\n'.join([
        'Going to create discount codes with the following details: ',
        f'Event: {event["name"]["text"]}',
        f'Ticket class: {ticket_class["name"]}',
        discount_confirmation,
        f'Prefix: {prefix}',
        f'Quantity: {quantity}',
        f'Uses per code: {uses}',
        'Proceed',
    ])):
        print('Exiting without creating tickets.')
        return

    codes = set()
    while len(codes) < quantity:
        code = ''.join(random.choice(string.ascii_uppercase) for _ in range(6))
        code = f'{prefix}-{code}'
        response = session.post(
            f'{EVENTBRITE_API_BASE}/discounts/',
            json={
                'discount.code': code,
                'discount.event_id': event['id'],
                'discount.ticket_class_ids': [ticket_class['id']],
                'discount.quantity_available': uses,
                **discount_options,
            },
        )
        # Only add the code to the set if it was created successfully.
        # This avoids issues like duplicates.
        if 200 <= response.status_code < 300:
            codes.add(code)

    print('Created the following codes:')
    print('\n'.join(codes))


@arg('--format', default='table', choices=('csv', 'table'))
def status(search='', format='table'):
    """Print the status of discount codes.

    Args:
        search: If provided, limit the output to only discount codes
            containing this string. Note: Eventbrite discount codes are
            case-insensitive.
        format: The format to print codes in. Supports tables for human
            readability and csv for machine readability.
    """
    search = search.lower()
    session = _make_session()
    event = _choose_event(session)
    types = ('discounts', 'access_codes')
    table = []
    for type_ in types:
        endpoint = f'{EVENTBRITE_API_BASE}/events/{event["id"]}/{type_}/'
        response = session.get(endpoint).json()
        while True:
            for code in response[type_]:
                if search in code['code'].lower():
                    table.append((
                        code['code'],
                        bool(code['quantity_sold']),
                        code['quantity_available'] - code['quantity_sold'],
                    ))

            continuation = response['pagination'].get('continuation')
            if continuation:
                response = session.get(
                    endpoint,
                    params={'continuation': continuation},
                ).json()
            else:
                break

    headers = ('code', 'used', 'remaining uses')
    if format == 'table':
        print(tabulate(table, headers))
    elif format == 'csv':
        csv.writer(sys.stdout).writerows((headers, *table))

if __name__ == '__main__':
    dispatch_commands([generate, status])
