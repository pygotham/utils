# Discount code generator

Creates and checks eventbrite discount and access codes.

Interactive, prompts the user for details upon execution. Install requirements
with `pip install -r requirements.txt`, set the `EVENTBRITE_TOKEN` environment
variable (found on https://www.eventbrite.com/myaccount/apps/; click "Show
client secret and API keys" then copy the "Private API Key").

Create codes using `./discount_codes.py generate` and follow the prompts.

To check the status of codes, use `./discount_codes.py status`:

```
usage: discount_codes.py status [-h] [-s SEARCH] [--format {csv,table}]

Print the status of discount codes.

    Args:
        search: If provided, limit the output to only discount codes
            containing this string. Note: Eventbrite discount codes are
            case-insensitive.
        format: The format to print codes in. Supports tables for human
            readability and csv for machine readability.


optional arguments:
  -h, --help            show this help message and exit
  -s SEARCH, --search SEARCH
                        ''
  --format {csv,table}  'table'
```

Requres Python 3.6 or greater.
