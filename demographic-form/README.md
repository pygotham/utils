# Update Demographic Info Mail Merge Sheet

We use a google sheet to track the speakers who have been emailed or need to be
emailed the speaker demographic information survey. The script
`update_emails_googlesheet.py` in this directory aids in updating that
spreadsheet.

The spreadsheet has Name, Email, Salt, Speaker ID, and Emailed On columns.
The salt is 16 randomly-generated alphanumeric characters. The speaker ID is
the first 8 digits of a SHA256 hash of the salt+email. (Some earlier rows
may use a different hash algorithm; the important thing is that they are
unique across the set of speakers).

## Using the Tool

1. Follow the steps at
   https://developers.google.com/sheets/api/quickstart/python to get
   credentials set up.
2. Install the requirements from `requirements.txt`.
3. Download the current speaker information from
   https://www.papercall.io/cfps/932/submissions
4. Run something like:
    ```
    python update_emails_googlesheet path/to/downloaded.csv https://google-sheet-url/...
    ```
