from hashlib import sha256
import argparse
import csv
import io
import os
import random
import re
import string

from apiclient import discovery
from oauth2client import client, tools
from oauth2client.file import Storage
import httplib2


# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/sheets.googleapis.com-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Sheets API Python Quickstart'


def get_credentials(opts):
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'sheets.googleapis.com-python-quickstart.json')
    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        credentials = tools.run_flow(flow, store, flags=opts)
        print('Storing credentials to ' + credential_path)
    return credentials


def parse_spreadsheet_id(spreadsheet_url):
    match = re.match("https://docs.google.com/spreadsheets/d/([^/]+)/edit(:?#[^/]+)?$", spreadsheet_url)
    if match:
        return match.group(1)
    raise ValueError("%r did not look like a google sheets URL" % spreadsheet_urL)


def main(papercall_csv, spreadsheet_id, credentials):
    http = credentials.authorize(httplib2.Http())
    discoveryUrl = "https://sheets.googleapis.com/$discovery/rest?version=v4"
    service = discovery.build(
        "sheets",
        "v4",
        http=http,
        discoveryServiceUrl=discoveryUrl,
    )

    call = service.spreadsheets().values().get(
        spreadsheetId=spreadsheet_id,
        range="A1:E1000",  # TODO: is there a better way to do this?
    )
    call = call.execute()
    rows = iter(call.get("values"))

    columns = next(rows)
    seen_rows = [dict(zip(columns, row)) for row in rows]
    seen_emails = set(row["Email"].strip().lower() for row in seen_rows)
    seen_hashes = set(row["Speaker ID"].strip() for row in seen_rows)
    last_row = len(seen_rows) + 1  # for header

    new_speakers = []
    new_speaker_emails = set()
    reader = csv.reader(io.open(papercall_csv, encoding="utf8"))
    headers = next(reader)
    for row in reader:
        data = dict(zip(headers, row))
        email = data["email"].strip().lower()
        if email not in seen_emails and email not in new_speaker_emails:
            new_speakers.append(data)
            new_speaker_emails.add(email)

    rows_to_add = []
    for speaker in new_speakers:
        while True:
            salt = "".join(random.choice(string.ascii_letters + string.digits) for _ in range(16))
            speaker_id = sha256((salt + speaker["email"]).encode("utf8")).hexdigest()[:8]
            if speaker_id not in seen_hashes:
                seen_hashes.add(speaker_id)
                break

        rows_to_add.append((
            speaker["name"],
            speaker["email"],
            salt,
            speaker_id,
            "",  # emailed-on date
        ))

    if rows_to_add:
        call = service.spreadsheets().values().append(
            spreadsheetId=spreadsheet_id,
            range="A1:E1000",  # it uses this range to find the table, so we don't have to do row math
            body={
                "range": "A1:E1000",
                "values": rows_to_add,
            },
            valueInputOption="RAW",
        )
        call.execute()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Update spreadsheet to track who has received demographic form emails",
        parents=[tools.argparser]
    )
    parser.add_argument("papercall_csv")
    parser.add_argument("google_sheet_url")

    opts = parser.parse_args()

    creds = get_credentials(opts)
    sheet_id = parse_spreadsheet_id(opts.google_sheet_url)

    main(opts.papercall_csv, sheet_id, creds)
