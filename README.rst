==============
PyGotham Utils
==============

This repository consists of scripts and other tools used to help move things
around during the course of planning, running, and winding down from PyGotham_.

If you'd like to add something new, create a new directory and place everything
inside it. This should include all code, assets, documentation (including
READMEs), and license information (if separate from the one found in the root
of this repository).

.. _PyGotham: https://pygotham.org
