"""Import the schedule from a CSV file.

Usage: python import.py path_to_talks
"""

import collections
import csv
import datetime
import os
import sys
from typing import Dict

import frontmatter
from slugify import slugify

SPECIAL_SLOTS = {
    'Breakfast': 'meal',
    'Break': 'break',
    'Lunch': 'meal',
    'Opening Remarks': 'remarks',
    'Closing Remarks': 'remarks',
    'The Ministry of Silly Talks': 'lightning',
}

IN_SKYTOP = {'break', 'meal'}


def parse_date(row: Dict[str, str]) -> datetime.datetime:
    return datetime.datetime.strptime(
        f"{row['Date']} {row['Time']} -0400",
        '%m/%d/%Y %I:%M:%S %p %z'
    )


def main(csv_file_name: str, talk_directory_name: str) -> None:
    # Non-talks slots (e.g., meals) don't have unique title but need to
    # have unique filenames. We need to keep track of how many times
    # we've seen each kind of slot so that we can include a unique
    # number in the filename.
    counters = collections.defaultdict(int)

    with open(csv_file_name) as f:
        reader = csv.DictReader(f)

        all_rows = list(reader)
        for i, row in enumerate(all_rows):
            slot = parse_date(row)

            try:
                next_row = all_rows[i + 1]
            except IndexError:
                duration = 15  # closing remarks
            else:
                if row['Date'] == next_row['Date']:
                    next_slot = parse_date(next_row)
                    duration = int((next_slot - slot).seconds / 60)
                else:
                    duration = 30  # lightning talks

            # TODO(dirn): The room names should be moved to header rows
            # in the CSV file.
            for room in ('PennTop South', 'PennTop North', 'Madison'):
                type_ = 'talk'

                title = row[room]
                if not title:
                    continue

                if title[:7] == 'Keynote':
                    type_ = 'keynote'


                slug = slugify(title)
                if title in SPECIAL_SLOTS:
                    type_ = SPECIAL_SLOTS[title]
                    if type_ in IN_SKYTOP:
                        room = 'Skytop'

                    counters[slug] += 1
                    slug = f'{slug}-{counters[slug]}'

                file_name = os.path.join(talk_directory_name, f'{slug}.md')

                if title in SPECIAL_SLOTS:
                    post = frontmatter.Post("")
                    post["title"] = title
                    with open(file_name, 'wb') as file_to_write:
                        frontmatter.dump(post, file_to_write)

                talk = frontmatter.load(file_name)

                talk['slot'] = slot
                talk['room'] = room
                talk['type'] = type_
                talk['duration'] = duration

                # TODO(dirn): I believe I did this because the object
                # returned by frontmatter.load didn't support checking
                # for the presence of a key, but I should confirm that
                # and change this if it does.
                try:
                    talk['title']
                except KeyError:
                    talk['title'] = title

                with open(file_name, 'wb') as file_to_write:
                    frontmatter.dump(talk, file_to_write)

if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])
