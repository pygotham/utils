==================
Talk Time Importer
==================

This script can be used to import the times for each talk into its markdown
file.

The CSV file should be in the format::

    Date,Time,Room 1,Room 2,Room 3
    10/06/2017,08:00:00 am,Breakfast
    10/06/2017,09:00:00 am,Opening Remarks
    10/06/2017,10:00:00 am,A Talk,Another Talk,Yet Another Talk

The schedule should include all days' talks, if applicable.

The script can be run with::

    $ python csv_import.py schedule.csv /path/to/site/_talks
