"""
Update YouTube titles and descriptions with data from ../_talks/.

Assumes that each of the talks already has a "video_url" attribute
formatted as a youtu.be short link.
"""
import argparse
import os.path
import re
from datetime import timedelta

import frontmatter
import pytz
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow


parser = argparse.ArgumentParser(
    description="Update talk descriptions on YouTube",
    epilog=(
        "Before using this script, you will need to generate and download "
        "a client_secret.json file from the Google developer console. Go "
        "to https://console.developers.google.com/ using your PyGotham "
        "account. Select an appropriate project, then click 'Credentials' "
        "on the left rail. Click 'Create Credentials', then 'OAuth Client "
        "ID', and choose the 'Desktop app' application type. Next, to "
        "accommodate the use of YouTube Brand Accounts, navigate to the "
        "OAuth Consent Screen page, choose "Make external" and select "
        "'Testing' for the publishing status, and add your account as an "
        "authorized test user. Finally, navigate back to the Credentials "        
        "page, click the download link, and save the file as "
        "'client_secret.json' in this directory. When running the update "
        "script, you will be prompted to log in. Log in with your "
        "PyGotham account and choose the appropriate brand account for "
        "the conference year on the account switcher screen."
    ),
)
parser.add_argument("talks_dir", metavar="TALKS_DIR", help="Directory containing one .md file per talk")
opts = parser.parse_args()


# The CLIENT_SECRETS_FILE variable specifies the name of a file that contains
# the OAuth 2.0 information for this application, including its client_id and
# client_secret.
CLIENT_SECRETS_FILE = "client_secret.json"

# This OAuth 2.0 access scope allows for full read/write access to the
# authenticated user's account and requires requests to use an SSL connection.
SCOPES = ['https://www.googleapis.com/auth/youtube.force-ssl']
API_SERVICE_NAME = 'youtube'
API_VERSION = 'v3'

def get_authenticated_service():
    flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRETS_FILE, SCOPES)
    credentials = flow.run_local_server()
    return build(API_SERVICE_NAME, API_VERSION, credentials = credentials)


def slugify(title):
    return re.sub("  *", " ", re.sub("[^a-zA-Z0-9 ]", " ", title)).lower().strip().replace(" ", "-")


def un_markdown(blob):
    link = re.compile(r"(\[[^\]]+\]\([^\)]+\))")
    out = []
    for part in link.split(blob):
        if link.match(part):
            out.append(part.replace("[", "").replace("]", " "))
        else:
            out.append(part)
    return "".join(out)


service = get_authenticated_service()

playlist_id = service.channels().list(
    mine=True,
    part="contentDetails",
).execute()["items"][0]["contentDetails"]["relatedPlaylists"]["uploads"]

videos_result = service.playlistItems().list(
    part="snippet",
    playlistId=playlist_id,
    maxResults=50,
).execute()
videos = videos_result["items"]

while videos_result.get("nextPageToken"):
    videos_result = service.playlistItems().list(
        part="snippet",
        playlistId=playlist_id,
        pageToken=videos_result["nextPageToken"],
        maxResults=50,
    ).execute()
    videos.extend(videos_result["items"])

for video in videos:
    title = video["snippet"]["title"]
    slug = slugify(title)
    video_id = video["snippet"]["resourceId"]["videoId"]

    talk_path = os.path.join(opts.talks_dir, slug + ".md")
    if not os.path.exists(talk_path):
        print("NO TALK: ", talk_path)
        continue

    with open(talk_path) as fp:
        parsed = frontmatter.load(fp)

    parsed["video_url"] = f"https://youtu.be/{video_id}"


    # work around bug in frontmatter :(
    adjusted_timeslot = parsed["slot"] - timedelta(hours=4)
    parsed["slot"] = adjusted_timeslot.astimezone(pytz.timezone("US/Eastern"))
    parsed["presentation_url"] = parsed["presentation_url"] or ""

    with open(talk_path, "wb") as fp:
        frontmatter.dump(parsed, fp)

    video = service.videos().list(
        id=video_id,
        part="snippet",
    ).execute()["items"][0]

    snippet = video["snippet"]

    speakers = parsed.get("speakers")
    description = ""
    if len(speakers) == 1:
        description = "Speaker: %s\n\n" % speakers[0]
    elif speakers:
        description = "Speakers: %s\n\n" % ", ".join(speakers)

    description += un_markdown(parsed.content)
    description = description.replace("<tt>", "").replace("</tt>", "")
    snippet["description"] = description
    snippet["title"] = title

    try:
        service.videos().update(
            part="snippet",
            body=dict(
                id=video_id,
                snippet=snippet,
            ),
        ).execute()
        print("updated", title)
    except Exception:
        print("COULD NOT UPDATE", title)
        print("---")
        print(repr(snippet))
        print("---")
